package kvstore

import akka.actor.Props
import akka.actor.Actor
import akka.actor.ActorRef
import scala.concurrent.duration._

object Replicator {
  case class Replicate(key: String, valueOption: Option[String], id: Long)
  case class Replicated(key: String, id: Long)
  
  case class Snapshot(key: String, valueOption: Option[String], seq: Long)
  case class SnapshotAck(key: String, seq: Long)

  def props(replica: ActorRef): Props = Props(new Replicator(replica))
}

class Replicator(val replica: ActorRef) extends Actor {
  import Replicator._
  import Replica._
  import context.dispatcher
  
  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // map from sequence number to pair of sender and request
  var acks = Map.empty[Long, (ActorRef, Replicate)]
  // a sequence of not-yet-sent snapshots (you can disregard this if not implementing batching)
  var pending = Vector.empty[Snapshot]
  
  var _seqCounter = 0L
  def nextSeq = {
    val ret = _seqCounter
    _seqCounter += 1
    ret
  }

  private case object Resend

  /* TODO Behavior for the Replicator. */
  def receive: Receive = {
    case request @ Replicate(key, valueOption, id) => {
      //(acks get nextSeq)
      // send Snapshot(key, valueOption, seq) to replica, but where is replica !?
      val seq = nextSeq
      acks += (seq -> (sender, request))
      replica ! Snapshot(key, valueOption, seq)
    }
    
    case SnapshotAck(key, seq) => {
      // send Replicated(key, id) to whoever send Replicate
      val (origSender: ActorRef, request: Replicate) = acks(seq)
      acks -= seq
      origSender ! Replicated(key, seq)
    }
    
    case Resend => acks foreach {
      case (seq: Long, (origSender: ActorRef, request: Replicate)) => {
        request match {
          case Replicate(key, valueOption, id) => replica ! Snapshot(key, valueOption, seq)
        }
      }
    }
    
  }
  
  // Resend snapshots every 100 ms
  context.system.scheduler.schedule(
      Duration(100, MILLISECONDS), Duration(100, MILLISECONDS), self, Resend)
}
