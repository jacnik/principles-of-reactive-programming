package kvstore

import akka.actor.{ OneForOneStrategy, Props, ActorRef, Actor }
import kvstore.Arbiter._
import scala.collection.immutable.Queue
import akka.actor.SupervisorStrategy.Restart
import scala.annotation.tailrec
import akka.pattern.{ ask, pipe }
import akka.actor.Terminated
import scala.concurrent.duration._
import akka.actor.PoisonPill
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy
import akka.util.Timeout

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor {
  import Replica._
  import Replicator._
  import Persistence._
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */
  
  var kv = Map.empty[String, String]
  // a map from secondary replicas to replicators
  var secondaries = Map.empty[ActorRef, ActorRef]
  // the current set of replicators
  var replicators = Set.empty[ActorRef]
  
  arbiter ! Join
  var currentSeq = 0L
  val persistence = context.actorOf(persistenceProps)//new Persistence(true)
  
  def receive = {
    case JoinedPrimary   => context.become(leader)
    case JoinedSecondary => context.become(replica)
  }
  
  // map from id to sender
  var avaitingPersist = Map.empty[Long, (ActorRef, Persist)]
  
  /* TODO Behavior for  the leader role. */
  val leader: Receive = {
    case Insert(key: String, value: String, id: Long) => {
      kv = kv + (key -> value)
      context.system.scheduler.scheduleOnce(Duration.create(1, "second"), self, Timeout)
      //context.system.scheduler.scheduleOnce(Duration.create(1, "second"), sender, OperationFailed(id))
      avaitingPersist += id -> (sender, Persist(key, Option(value), id))
      persistence ! Persist(key, Option(value), id)
      //sender ! OperationAck(id)
    }
    case Remove(key: String, id: Long) => {
      kv = kv - key
     avaitingPersist += id -> (sender, Persist(key, None, id))
      persistence ! Persist(key, None, id)
      //sender ! OperationAck(id)
    }
    case Get(key: String, id: Long) => {
      sender ! GetResult(key, kv get key, id)
    }
    
    case Persisted(key, id) => {   
      avaitingPersist(id)._1 ! OperationAck(id)
      avaitingPersist -= id
    }
        
    case Replicas(set) => set - self foreach {secondary: ActorRef =>
      if( !(secondaries contains secondary)) {
        val replicator = context.actorOf(Replicator props secondary)
        secondaries += secondary -> replicator
        var id = 0L
        kv foreach {case (key, value) => 

            replicator ! Replicate(key, Option(value), id)
            //persistence ! Persist(key, Option(value), id)
            id += 1
        }
      }
    }
    
    case Timeout => {
        avaitingPersist foreach { case (k, v) => v._1 ! OperationFailed(k) }
        //context stop self
      }
  }
  
  
  /* TODO Behavior for the replica role. */
  val replica: Receive = {
    case Get(key: String, id: Long) => {
      sender ! GetResult(key, kv get key, id)
    }
    
    case Snapshot(key, valueOption, seq) => {
      if (seq < currentSeq) sender ! SnapshotAck(key, seq)
      else if (seq > currentSeq) ()
      else {
        valueOption match {
          case Some(v) => kv = kv + (key -> v)
          case None => kv = kv - key
        }
        avaitingPersist += seq -> (sender, Persist(key, valueOption, seq))
        persistence ! Persist(key, valueOption, seq)
        //sender ! SnapshotAck(key, seq)
        currentSeq += 1
      }
    }
    
    case Persisted(key, id) => {   
      avaitingPersist(id)._1 ! SnapshotAck(key, id)
      avaitingPersist -= id
    }
  }
  
  // Retry persistance every 100 ms
  context.system.scheduler.schedule(Duration(100, MILLISECONDS), Duration(100, MILLISECONDS)) {
    avaitingPersist foreach { case (k, v) => persistence ! v._2 }
  }
}

