package nodescala

import scala.language.postfixOps
import scala.util.{Try, Success, Failure}
import scala.collection._
import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.async.Async.{async, await}
import org.scalatest._
import NodeScala._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class NodeScalaSuite extends FunSuite {

  test("A Future should always be completed") {
    val always = Future.always(517)

    assert(Await.result(always, 0 nanos) == 517)
  }
  test("A Future should never be completed") {
    val never = Future.never[Int]

    try {
      Await.result(never, 1 second)
      assert(false)
    } catch {
      case t: TimeoutException => // ok!
    }
  }
  
   test("all") {
     val list = List(Future(1), Future(2), Future(3))
     //val future = Future(List(1, 2, 3))
     val res = List(1, 2, 3)
     
     val l = Future.all(list)
     for {
        x <- l
     } assert(x == res)
    
   }

  test("any") {
     val t = Future.any(List(Future { 1 }, Future { 2 }, Future { throw new Exception }))
     
//     for {
//        f <- t
//     } println(f)
     
     assert(true)
   }
    
  test("any method works HAPPY") {
    val l = List(Future { Thread.sleep(200); 200 }, Future { Thread.sleep(150); 150 }, Future { Thread.sleep(199); 199 })
    val duration = 100 millis
    val r = Await.result(Future.any(l), 300 millis)
    assert(r == 150)
    
   val l1 = List(
        Future { Thread.sleep(299); 299 },
        Future { Thread.sleep(299); 299 },
        Future { Thread.sleep(50); 50 },
        Future { Thread.sleep(100); 100 }, 
        Future { Thread.sleep(70); 70 }) // why this is not first?
    val r1 = Await.result(Future.any(l1), 300 millis)
    assert(r1 == 50)
  }

  test("delay") {
      //Future.delay(1 second) onComplete { case _ => println("a") }
  }

  test("run") {
      val working = Future.run() { ct =>
      Future {
        while (ct.nonCancelled) {
          println("working")
        }
        //println("done")
      }
    }
    Future.delay(5 seconds) onSuccess {
      case _ => working.unsubscribe()
    }
  }
  
  test("should cancel computation eventually") {
    val p = Promise[Int]()
    val working = Future.run() {
      ct => Future {
        while (ct.nonCancelled) {
          println("working")
        }
        //println("done")
        p.success(1)
      }
    }
    Future.delay(1 seconds) onSuccess {
      case _ => working.unsubscribe()
    }
    assert(Await.result(p.future, Duration.Inf) == 1)
  }
    
  class DummyExchange(val request: Request) extends Exchange {
    @volatile var response = ""
    val loaded = Promise[String]()
    def write(s: String) {
      response += s
    }
    def close() {
      loaded.success(response)
    }
  }

  class DummyListener(val port: Int, val relativePath: String) extends NodeScala.Listener {
    self =>

    @volatile private var started = false
    var handler: Exchange => Unit = null

    def createContext(h: Exchange => Unit) = this.synchronized {
      assert(started, "is server started?")
      handler = h
    }

    def removeContext() = this.synchronized {
      assert(started, "is server started?")
      handler = null
    }

    def start() = self.synchronized {
      started = true
      new Subscription {
        def unsubscribe() = self.synchronized {
          started = false
        }
      }
    }

    def emit(req: Request) = {
      val exchange = new DummyExchange(req)
      if (handler != null) handler(exchange)
      exchange
    }
  }

  class DummyServer(val port: Int) extends NodeScala {
    self =>
    val listeners = mutable.Map[String, DummyListener]()

    def createListener(relativePath: String) = {
      val l = new DummyListener(port, relativePath)
      listeners(relativePath) = l
      l
    }

    def emit(relativePath: String, req: Request) = this.synchronized {
      val l = listeners(relativePath)
      l.emit(req)
    }
  }
  test("Server should serve requests") {
    val dummy = new DummyServer(8191)
    val dummySubscription = dummy.start("/testDir") {
      request => for (kv <- request.iterator) yield (kv + "\n").toString
    }

    // wait until server is really installed
    Thread.sleep(500)

    def test(req: Request) {
      val webpage = dummy.emit("/testDir", req)
      val content = Await.result(webpage.loaded.future, 1 second)
      val expected = (for (kv <- req.iterator) yield (kv + "\n").toString).mkString
      assert(content == expected, s"'$content' vs. '$expected'")
    }

    test(immutable.Map("StrangeRequest" -> List("Does it work?")))
    test(immutable.Map("StrangeRequest" -> List("It works!")))
    test(immutable.Map("WorksForThree" -> List("Always works. Trust me.")))

    dummySubscription.unsubscribe()
  }
  
  test("Server should be stoppable if receives infinite  response") {
    val dummy = new DummyServer(8191)
    val dummySubscription = dummy.start("/testDir") {
      request => Iterator.continually("a")
    }

    // wait until server is really installed
    Thread.sleep(500)

    val webpage = dummy.emit("/testDir", Map("Any" -> List("thing")))
    try {
      // let's wait some time
      Await.result(webpage.loaded.future, 1 second)
      fail("infinite response ended")
    } catch {
      case e: TimeoutException =>
    }

    // stop everything
    dummySubscription.unsubscribe()
    Thread.sleep(500)
    webpage.loaded.future.now // should not get NoSuchElementException
  }
}




