package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double],
      c: Signal[Double]): Signal[Double] = {
    Signal {
      math.pow(b(), 2) - 4*(a()*c())
    }
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
      c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    Signal {
      val d = delta()
      val a_ = a()
      val b_ = b()
      if (d < 0) Set()
      else if (d == 0) Set( -b_ / (2*a_) )
      else Set( (-b_ - math.sqrt(d)) / (2*a_), (-b_ + math.sqrt(d)) / (2*a_))
    }
  }
}
