/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import scala.collection.immutable.Queue

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection*/
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply
  
  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor {
  import BinaryTreeSet._
  import BinaryTreeNode._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root = createRoot

  // optional
  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal
  
  // optional
  /** Accepts `Operation` and `GC` messages. */
   val normal: Receive = { 
//      case Insert(requester, id, elem) => {
//        println (s"got insert $id")
//        root ! Insert(requester, id, elem)
//      }
//      case Contains(requester, id, elem) => {
//        println (s"got contains $id")
//        root ! Contains(requester, id, elem)
//      }
//      case Remove(requester, id, elem) => {
//        println (s"got remove $id")
//        root ! Remove(requester, id, elem)
//      }
      
      case message: Operation => root ! message 
      
      case GC => {
        val newRoot = createRoot
        context.become(garbageCollecting(newRoot)) 
        root ! CopyTo(newRoot)  
      }
   }

  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = {
//    case message: Operation => {
//      println ("new root got message")
//      pendingQueue = pendingQueue enqueue message
//      //context become garbageCollecting(newRoot)
//    }
      case Insert(requester, id, elem) => {
        println (s"got insert $id")
        pendingQueue = pendingQueue enqueue Insert(requester, id, elem)
      }
      case Contains(requester, id, elem) => {
        println (s"got contains $id")
        pendingQueue = pendingQueue enqueue Contains(requester, id, elem)
      }
      case Remove(requester, id, elem) => {
        println (s"got remove $id")
        pendingQueue = pendingQueue enqueue Remove(requester, id, elem)
      }
      
    case CopyFinished => {
      context stop root
      root = newRoot
      context become normal
      pendingQueue foreach (message => root ! message)
      
    }
  }
}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode], elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  // optional
  def receive = normal
  
  
  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = { 
      case Insert(requester, id, newElem) => {
        // values on the left are SMALLER then elem
        if (elem > newElem) {
          /* add left */
          if (subtrees.contains(Left)) subtrees(Left) ! Insert(requester, id, newElem)
          else {
            subtrees += (Left -> context.actorOf(BinaryTreeNode.props(newElem, initiallyRemoved = false)))       
            requester ! OperationFinished(id)
          }
        }
        else if (elem < newElem) {
           /*add right*/
          if (subtrees.contains(Right)) subtrees(Right) ! Insert(requester, id, newElem)
          else {
            subtrees += (Right -> context.actorOf(BinaryTreeNode.props(newElem, initiallyRemoved = false)))                  
            requester ! OperationFinished(id)
          }
        } else {
          if (removed) removed = false
          requester ! OperationFinished(id)
        }
        //requester ! OperationFinished(id)
      }
      
      case Contains(requester, id, newElem) => {
        if (elem > newElem) {
          /* check left */
          if (subtrees.contains(Left)) subtrees(Left) ! Contains(requester, id, newElem)
          else requester ! ContainsResult(id, false)
        }
        else if (elem < newElem) {
           /* check right*/
          if (subtrees.contains(Right)) subtrees(Right) ! Contains(requester, id, newElem)
          else requester ! ContainsResult(id, false)
        }
        else requester ! ContainsResult(id, !removed)
        //context.stop(self)
      }
      
      case Remove(requester, id, newElem) => {
        if (elem > newElem) {
          /* check left */
          if (subtrees.contains(Left)) subtrees(Left) ! Remove(requester, id, newElem)
          else requester ! OperationFinished(id)
        }
        else if (elem < newElem) {
           /* check right*/
          if (subtrees.contains(Right)) subtrees(Right) ! Remove(requester, id, newElem)
          else requester ! OperationFinished(id)
        }
        else {
          if (!removed) removed = true
          requester ! OperationFinished(id)
        }
        //requester ! OperationFinished(id)
      }

      case CopyTo(actorRef) => {
         val expected = subtrees.map(_._2).toSet
         expected foreach (_ ! CopyTo(actorRef))
         //val id = self.hashCode()
         //if (!removed) CopyTo(actorRef).treeNode ! Insert(self, id, elem)
         if (!removed) actorRef ! Insert(self, elem, elem)
         //else context stop self
         if (expected.isEmpty && removed) {
           context.parent ! CopyFinished
           //context become normal
           //context become copying(expected, true)
         }
         else context become copying(expected, removed)
      }       
   }

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
   case OperationFinished(id) => {
     if (expected.isEmpty) {
       context.parent ! CopyFinished
       //self ! CopyFinished
       //context become normal
     }
     else context become copying(expected, true)

    }
    case CopyFinished => {
      if( (expected - sender).isEmpty && insertConfirmed) {
        context.parent ! CopyFinished
        //context stop self
        //context become normal
      }
      else context become copying(expected - sender, insertConfirmed)
    }
  }
    
}
