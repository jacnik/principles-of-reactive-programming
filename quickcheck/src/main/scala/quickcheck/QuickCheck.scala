package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("insert to empty and delete") = forAll { a: Int =>
    val h = insert(a, empty)
    isEmpty(deleteMin(h))
  }
    
  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }

  property("minimum of melding") = forAll { (h1: H, h2: H) =>
    //val m1 = if (findMin(h1) > findMin(h2)) findMin(h2) else findMin(h1)
    ////val m2 = if (isEmpty(h2)) 0 else findMin(h2)
    //findMin(meld(h1, h2)) == m1
    val res = meld(h1, h2) 
    if (isEmpty(h1) && isEmpty(h2)) isEmpty(res) 
    else {
      if (isEmpty(h1)) findMin(res) == findMin(h2) 
      else {
        if (isEmpty(h2)) findMin(res) == findMin(h1)
        else { // both are not empty
          val m = if (findMin(h1) > findMin(h2)) findMin(h2) else findMin(h1)
          findMin(res) == m
        }
      } 
    }
    //false
  }
  
 property("sorted sequence") = forAll { (h: H) =>
   
    def min(i: Int, xh: H): Boolean = {
      if (isEmpty(xh)) true else i <= findMin(xh) && min(findMin(xh), deleteMin(xh)) 
    }
    
    if (isEmpty(h)) true else min(findMin(h), deleteMin(h))
  }

   // check if two Heaps are identical
   def areEqual(xh1: H, xh2: H): Boolean = {
      if (isEmpty(xh1)) isEmpty(xh2) 
      else findMin(xh1) == findMin(xh2) && areEqual(deleteMin(xh1), deleteMin(xh2))
   }
     
  property("associative meld") = forAll { (h1: H, h2: H, h3: H) =>
    val r1 = meld(meld(h1, h2), h3)
    val r2 = meld(meld(h3, h2), h1)
    areEqual(r1, r2)
  }
  
  property("meld and delete min") = forAll { (h1: H, h2: H) =>
    if (isEmpty(h1) && isEmpty(h2)) isEmpty(meld(h1, h2))
    if (!isEmpty(h1) && !isEmpty(h2)) {
      val m1 = deleteMin(meld(h1, h2))
      val m2 = if (findMin(h1) > findMin(h2)) 
        meld(h1, deleteMin(h2)) else meld(deleteMin(h1), h2)
      areEqual(m1, m2)
    }
    true
  }
  
  property("list and order") = forAll { (h1: H, h2: H) =>
    val m = meld(h1, h2)
 
    def desparate(x1: H, x2: H, m1: H) : Boolean = {
      if (isEmpty(x1)) true
      if (isEmpty(m1)) isEmpty(x1) && isEmpty(x2)
      if (!isEmpty(x1) && !isEmpty(x2)) {
        val m = if (findMin(x1) > findMin(x2)) findMin(x2) else findMin(x1)
        findMin(m1) == m && desparate(deleteMin(x1), insert(m, x2), meld(x1, insert(m, x2)))
      }
      true
    }
    desparate(h1, h2, m) && desparate(h2, h1, m) 
  }
    
  lazy val genHeap: Gen[H] = for {
    i <- arbitrary[Int]
    isEmpty <- arbitrary[Boolean] 
    h <- if (isEmpty) const(empty) else genHeap
  } yield if (isEmpty) h else insert(i, h)
  
/*  lazy val genHeap: Gen[H] = for {
    i <- arbitrary[Int]
    h <- oneOf(const(empty), genHeap, genHeap, genHeap, genHeap)
  } yield insert(i, h)*/
  
    // print the content of a heap
  def printHeap(h: H) {
    if (!isEmpty(h)) {
      println(findMin(h) + " ")
      printHeap(deleteMin(h))
    }
  }
  
  // dummy logging property
/*  property("log") = forAll { (h: H) =>
    println("print heap: ")
    printHeap(h)
    true
  }*/
  
  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

}
